(function($){
	

	$.fn.slider = function(options) {
		var items = $('.slider__item');

	var bxSlider =	 $('#slider1').bxSlider({
			onSliderLoad: function(currentIndex) {
				$(items[currentIndex]).addClass('active').find('.text').show();
			},
			onSlideBefore: function($slideElement, oldIndex, newIndex) {
				$(items[oldIndex]).find('.text').hide();
				$(items[newIndex]).addClass('active');
				$(items[oldIndex]).removeClass('active');
			},
			onSlideAfter: function($slideElement, oldIndex, newIndex) {
				$(items[newIndex]).find('.text').fadeIn(900);
				
			}
		});

		
	}

})(jQuery);