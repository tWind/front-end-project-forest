(function($){
	

	$.fn.carousel = function(options) {
		var bxCarousel = $('#carousel').bxSlider({
			startSlide: 0,
			minSlides: 5,
			maxSlides: 5,
			slideWidth: 175,
			slideMargin: 5,
			pager: false,
			moveSlides: 1,
			infinityLoop: true,
			onSliderLoad: function(currentIndex) {
				var items = $('.carousel__item').not('.bx-clone');
				$(items[currentIndex+1])
					.css({ width: "450px"})
					.find('.text').css({ display: "block"});
			},
			onSlideBefore: function($slideElement, oldIndex, newIndex) {
				clipSlide(oldIndex);
				expandSlide(newIndex);
			},
			onSlideAfter: function($slideElement, oldIndex, newIndex) {
				
			},
			onSlideNext: function ($slideElement, oldIndex, newIndex) {
				
			}
		 });
		
		var items = $('.carousel__item').not('.bx-clone');

		function expandSlide(i) {
			$(items[i])
				.next()
					.animate({ width: '450px' },
						function() {
							$(this).find('.text').fadeIn();
						});
		}
		function clipSlide(i) {
			$(items[i]).next().animate({ width: '175px' },
						function() {
							$(this).find('.text').hide();
						});
		}

		$(window).load(function() {
			var wWidth = Math.max( $(window).width(), window.innerWidth);
			/*if(wWidth <= 1010) {
				bxCarousel.reloadSlider({
					startSlide: 0,
					minSlides: 3,
					maxSlides: 3,
					slideWidth: 175,
					slideMargin: 5,
					pager: false,
					moveSlides: 1,
					infinityLoop: true,
					onSliderLoad: function(currentIndex) {
						var items = $('.carousel__item').not('.bx-clone');
						$(items[currentIndex+1])
							.css({ width: "450px"})
							.find('.text').css({ display: "block"});
					},
					onSlideBefore: function($slideElement, oldIndex, newIndex) {
						clipSlide(oldIndex);
					},
					onSlideAfter: function($slideElement, oldIndex, newIndex) {
						expandSlide(newIndex);
					}
				 });
			}*/
			if(wWidth <= 1010) {
				bxCarousel.reloadSlider({
					startSlide: 1,
					pager: false,
					moveSlides: 1,
					infinityLoop: true,
					onSlideBefore: function($slideElement, oldIndex, newIndex) {
						$(items[oldIndex]).find('.text').fadeOut();
					},
					onSlideAfter: function($slideElement, oldIndex, newIndex) {
						$(items[newIndex]).find('.text').css({display: 'block'});
					}
				 });
			}
		}) 

	}

})(jQuery);