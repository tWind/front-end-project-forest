(function($){

	$.fn.tabs = function(options) {
		var defaults = {
			templates: ".tabs__content"
		}
		var buttons = $(this).children();
		var tabs = $(defaults.templates).find('.item');

		$(buttons).on('click', function() {
			$(buttons).removeClass('active');
			$(this).addClass('active');
			
			var i = $(this).index();
			$(tabs).hide();
			$(tabs[i]).show();
		})
	}

})(jQuery);