(function($){

	$.fn.dropDown = function(options) {
		var defaults = {
			buttonClass: 'main-nav__item',
			subPaneClass: 'main-nav__dropdown'
		}
		var mainPane = this;
		var menu = {};

		

		menu.init = function() {
			// apply options
			this.settings =  $.extend({}, defaults, options);
			this.items = $(mainPane).find('.'+ this.settings.buttonClass);
			this.subPanes = $(this.items).find('.'+ this.settings.subPaneClass);
			
			menu.action();
		}
		menu.action = function() {
			$(menu.items).on('click', function() {
				var currentSubPane = $(this).find(menu.subPanes);

				($(this).hasClass('active') == true) ? removeCurrent(this) : makeCurrent(this);

				function makeCurrent(el) {
					$(menu.items).removeClass('active');
					$(el).addClass('active');
					$(menu.subPanes).fadeOut('fast');
					$(currentSubPane).fadeIn('fast');
				}
				function removeCurrent(el) {
					$(menu.items).removeClass('active');
					$(menu.subPanes).fadeOut('fast');
				}
				return false;
			});

		}
		menu.init();
	}

})(jQuery);