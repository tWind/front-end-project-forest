(function($){	

	$.fn.motion = function(options) {
		
		var viewport = this;
		var img = $(viewport).find('img');
		var wWidth = Math.max( $(window).width(), window.innerWidth);
		console.log(wWidth);
		var pane = {};

		
		
		
		pane.init = function() {
			this.height = $(img).height();
			this.posLeft = parseInt(img.css('left'));
			this.minRange = pane.posLeft - ($(img).width()-wWidth)/2;
			this.maxRange = pane.posLeft + ($(img).width()-wWidth)/2;
			pane.build();
		}
		pane.build = function() {
			$(img).css({ 'margin-left': -$(img).width()/2 });
			$(viewport).css({
				height: this.height,
				overflow: 'hidden'
			});
			$(viewport).wrap('<div class="motion__wrapper"></div>');
		}
		move = {
			start: function(event) {
				this.posX = event.pageX;
				this.limit = move.posX / wWidth * 100;
				
				move.limit < 50 ? move.left() : move.right();				
			},
			stop: function() {
				clearInterval(move.interval);
			},
			left: function() {
				move.interval = setInterval(function() {
					if(pane.minRange < Math.abs(pane.posLeft)) {
						pane.posLeft -= 1;
						$(img).css({left: pane.posLeft})	
					}					
				}, 10);
			},
			right: function() {
				move.interval = setInterval(function() {
					if (pane.maxRange > Math.abs(pane.posLeft)) {
						pane.posLeft += 1;
						$(img).css({left: pane.posLeft});
					}
				}, 10);
			}
		}

		pane.init();

		$(viewport).on('mousemove', function(event) {
			move.stop();
			move.start(event);
		});
		$(viewport).on('mouseout', function(event) {
			move.stop();
		});
		
		
	}

})(jQuery);