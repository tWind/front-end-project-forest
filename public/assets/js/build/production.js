(function($){

	$.fn.dropDown = function(options) {
		var defaults = {
			buttonClass: 'main-nav__item',
			subPaneClass: 'main-nav__dropdown'
		}
		var mainPane = this;
		var menu = {};

		

		menu.init = function() {
			// apply options
			this.settings =  $.extend({}, defaults, options);
			this.items = $(mainPane).find('.'+ this.settings.buttonClass);
			this.subPanes = $(this.items).find('.'+ this.settings.subPaneClass);
			
			menu.action();
		}
		menu.action = function() {
			$(menu.items).on('click', function() {
				var currentSubPane = $(this).find(menu.subPanes);

				($(this).hasClass('active') == true) ? removeCurrent(this) : makeCurrent(this);

				function makeCurrent(el) {
					$(menu.items).removeClass('active');
					$(el).addClass('active');
					$(menu.subPanes).fadeOut('fast');
					$(currentSubPane).fadeIn('fast');
				}
				function removeCurrent(el) {
					$(menu.items).removeClass('active');
					$(menu.subPanes).fadeOut('fast');
				}
				return false;
			});

		}
		menu.init();
	}

})(jQuery);
(function($){
	

	$.fn.carousel = function(options) {
		var bxCarousel = $('#carousel').bxSlider({
			startSlide: 0,
			minSlides: 5,
			maxSlides: 5,
			slideWidth: 175,
			slideMargin: 5,
			pager: false,
			moveSlides: 1,
			infinityLoop: true,
			onSliderLoad: function(currentIndex) {
				var items = $('.carousel__item').not('.bx-clone');
				$(items[currentIndex+1])
					.css({ width: "450px"})
					.find('.text').css({ display: "block"});
			},
			onSlideBefore: function($slideElement, oldIndex, newIndex) {
				clipSlide(oldIndex);
				expandSlide(newIndex);
			},
			onSlideAfter: function($slideElement, oldIndex, newIndex) {
				
			},
			onSlideNext: function ($slideElement, oldIndex, newIndex) {
				
			}
		 });
		
		var items = $('.carousel__item').not('.bx-clone');

		function expandSlide(i) {
			$(items[i])
				.next()
					.animate({ width: '450px' },
						function() {
							$(this).find('.text').fadeIn();
						});
		}
		function clipSlide(i) {
			$(items[i]).next().animate({ width: '175px' },
						function() {
							$(this).find('.text').hide();
						});
		}

		$(window).load(function() {
			var wWidth = Math.max( $(window).width(), window.innerWidth);
			/*if(wWidth <= 1010) {
				bxCarousel.reloadSlider({
					startSlide: 0,
					minSlides: 3,
					maxSlides: 3,
					slideWidth: 175,
					slideMargin: 5,
					pager: false,
					moveSlides: 1,
					infinityLoop: true,
					onSliderLoad: function(currentIndex) {
						var items = $('.carousel__item').not('.bx-clone');
						$(items[currentIndex+1])
							.css({ width: "450px"})
							.find('.text').css({ display: "block"});
					},
					onSlideBefore: function($slideElement, oldIndex, newIndex) {
						clipSlide(oldIndex);
					},
					onSlideAfter: function($slideElement, oldIndex, newIndex) {
						expandSlide(newIndex);
					}
				 });
			}*/
			if(wWidth <= 1010) {
				bxCarousel.reloadSlider({
					startSlide: 1,
					pager: false,
					moveSlides: 1,
					infinityLoop: true,
					onSlideBefore: function($slideElement, oldIndex, newIndex) {
						$(items[oldIndex]).find('.text').fadeOut();
					},
					onSlideAfter: function($slideElement, oldIndex, newIndex) {
						$(items[newIndex]).find('.text').css({display: 'block'});
					}
				 });
			}
		}) 

	}

})(jQuery);
(function($){

	$.fn.dropDown = function(options) {
		var defaults = {
			buttonClass: 'main-nav__item',
			subPaneClass: 'main-nav__dropdown'
		}
		var mainPane = this;
		var menu = {};

		

		menu.init = function() {
			// apply options
			this.settings =  $.extend({}, defaults, options);
			this.items = $(mainPane).find('.'+ this.settings.buttonClass);
			this.subPanes = $(this.items).find('.'+ this.settings.subPaneClass);

			menu.action();
		}
		menu.action = function() {
			$(menu.items).on('mouseover', function() {
				var currentSubPane = $(this).find(menu.subPanes);

				($(this).hasClass('active') == true) ? removeCurrent(this) : makeCurrent(this);

				function makeCurrent(el) {
					$(menu.items).removeClass('active');
					$(el).addClass('active');
					$(menu.subPanes).slideUp('fast');
					$(currentSubPane).slideDown('fast');
				}
				function removeCurrent(el) {
					$(menu.items).removeClass('active');
					$(menu.subPanes).slideUp('fast');
				}
				return false;
			});

		}
		menu.init();
	}

})(jQuery);
(function($){	

	$.fn.motion = function(options) {
		
		var viewport = this;
		var img = $(viewport).find('img');
		var wWidth = Math.max( $(window).width(), window.innerWidth);
		console.log(wWidth);
		var pane = {};

		
		
		
		pane.init = function() {
			this.height = $(img).height();
			this.posLeft = parseInt(img.css('left'));
			this.minRange = pane.posLeft - ($(img).width()-wWidth)/2;
			this.maxRange = pane.posLeft + ($(img).width()-wWidth)/2;
			pane.build();
		}
		pane.build = function() {
			$(img).css({ 'margin-left': -$(img).width()/2 });
			$(viewport).css({
				height: this.height,
				overflow: 'hidden'
			});
			$(viewport).wrap('<div class="motion__wrapper"></div>');
		}
		move = {
			start: function(event) {
				this.posX = event.pageX;
				this.limit = move.posX / wWidth * 100;
				
				move.limit < 50 ? move.left() : move.right();				
			},
			stop: function() {
				clearInterval(move.interval);
			},
			left: function() {
				move.interval = setInterval(function() {
					if(pane.minRange < Math.abs(pane.posLeft)) {
						pane.posLeft -= 1;
						$(img).css({left: pane.posLeft})	
					}					
				}, 10);
			},
			right: function() {
				move.interval = setInterval(function() {
					if (pane.maxRange > Math.abs(pane.posLeft)) {
						pane.posLeft += 1;
						$(img).css({left: pane.posLeft});
					}
				}, 10);
			}
		}

		pane.init();

		$(viewport).on('mousemove', function(event) {
			move.stop();
			move.start(event);
		});
		$(viewport).on('mouseout', function(event) {
			move.stop();
		});
		
		
	}

})(jQuery);
(function($){

	$.fn.dropDown = function(options) {
		var defaults = {
			switcher: '#menu-switch',
			itemClass: 'main-nav__item',
			subPaneClass: 'main-nav__dropdown',
			wrapperClass: 'main-nav__wrapper',
			lowBreakPoint: '970'
		}
		
		var mainPane = this;
		var menuWidth = $(mainPane).outerWidth();
		console.log(menuWidth);
		var menu = {};

		

		menu.init = function() {
			wWidth = Math.max( $(window).width(), window.innerWidth);

			// apply options
			this.settings =  $.extend({}, defaults, options);
			this.items = $(mainPane).find('.'+ this.settings.itemClass);
			this.subPanes = $(this.items).find('.'+ this.settings.subPaneClass);
			this.wrapper = $(mainPane).parent();
			
			menu.build(wWidth, menuWidth);
		}
		menu.build = function() {
			if(wWidth >= this.settings.lowBreakPoint) {
				menu.clear();
				$(mainPane).css({left: ""});
			} else {
				$(mainPane).parent().addClass(menu.settings.wrapperClass);
			}
			
		}
		menu.clear = function() {
			$('body').attr('style', "").removeClass('pinned');
			$(this.wrapper).attr('style', "");
			$(mainPane).parent().removeClass(menu.settings.wrapperClass);
			$(menu.settings.switcher).data('status', "on");
		}
		menu.methods = {
			makeCurrent: function(el, current) {
				$(menu.items).removeClass('active');
				$(el).addClass('active');
				$(menu.subPanes).slideUp('fast');
				$(current).slideDown('fast');
				return false;
			},
			removeCurrent: function(el, current) {
				$(menu.items).removeClass('active');
				$(menu.subPanes).slideUp('fast');
				return false;
			},
			switchMenu: {
				"on": function(button) {
					
					$(mainPane).css({left: -menuWidth});

					$(button).data('status', "off");
					$('body').addClass('pinned').animate({left: menuWidth}, 600);
					$(mainPane).stop().animate({ left: 0 }, 600);
					$(menu.wrapper).stop().fadeIn();
				},
				"off": function(button) {
					//console.log("off");
					$(button).data('status', "on");
					$('body').animate({left: 0}, 600, function() {
						$(this).removeClass('pinned');
					});
					$(mainPane).stop().animate({ left: -menuWidth });
					$(menu.wrapper).fadeOut();
				}
			}
		}

		menu.init();

		$(window).on('resize', function() {
			menu.init();
		});

		$(menu.settings.switcher).on('click', function() {
			menu.methods.switchMenu[$(this).data('status')](this);
		});

		$(menu.items).on('click', function() {
			var currentSubPane = $(this).find(menu.subPanes);
			($(this).hasClass('active') == true) 
				? menu.methods.removeCurrent(this, currentSubPane) 
				: menu.methods.makeCurrent(this, currentSubPane);
		});


	}

})(jQuery);
(function($){
	

	$.fn.slider = function(options) {
		var items = $('.slider__item');

	var bxSlider =	 $('#slider1').bxSlider({
			onSliderLoad: function(currentIndex) {
				$(items[currentIndex]).addClass('active').find('.text').show();
			},
			onSlideBefore: function($slideElement, oldIndex, newIndex) {
				$(items[oldIndex]).find('.text').hide();
				$(items[newIndex]).addClass('active');
				$(items[oldIndex]).removeClass('active');
			},
			onSlideAfter: function($slideElement, oldIndex, newIndex) {
				$(items[newIndex]).find('.text').fadeIn(900);
				
			}
		});

		
	}

})(jQuery);
(function($){

	$.fn.tabs = function(options) {
		var defaults = {
			templates: ".tabs__content"
		}
		var buttons = $(this).children();
		var tabs = $(defaults.templates).find('.item');

		$(buttons).on('click', function() {
			$(buttons).removeClass('active');
			$(this).addClass('active');
			
			var i = $(this).index();
			$(tabs).hide();
			$(tabs[i]).show();
		})
	}

})(jQuery);


// Avoid `console` errors in browsers that lack a console.
(function() {
var method;
var noop = function () {};
var methods = [
			'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
			'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
			'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
			'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
];
var length = methods.length;
var console = (window.console = window.console || {});

while (length--) {
			method = methods[length];

			// Only stub undefined methods.
			if (!console[method]) {
						console[method] = noop;
			}
}
}());

// Place any jQuery/helper plugins in here.
