(function($){

	$.fn.dropDown = function(options) {
		var defaults = {
			switcher: '#menu-switch',
			itemClass: 'main-nav__item',
			subPaneClass: 'main-nav__dropdown',
			wrapperClass: 'main-nav__wrapper',
			lowBreakPoint: '970'
		}
		
		var mainPane = this;
		var menuWidth = $(mainPane).outerWidth();
		console.log(menuWidth);
		var menu = {};

		

		menu.init = function() {
			wWidth = Math.max( $(window).width(), window.innerWidth);

			// apply options
			this.settings =  $.extend({}, defaults, options);
			this.items = $(mainPane).find('.'+ this.settings.itemClass);
			this.subPanes = $(this.items).find('.'+ this.settings.subPaneClass);
			this.wrapper = $(mainPane).parent();
			
			menu.build(wWidth, menuWidth);
		}
		menu.build = function() {
			if(wWidth >= this.settings.lowBreakPoint) {
				menu.clear();
				$(mainPane).css({left: ""});
			} else {
				$(mainPane).parent().addClass(menu.settings.wrapperClass);
			}
			
		}
		menu.clear = function() {
			$('body').attr('style', "").removeClass('pinned');
			$(this.wrapper).attr('style', "");
			$(mainPane).parent().removeClass(menu.settings.wrapperClass);
			$(menu.settings.switcher).data('status', "on");
		}
		menu.methods = {
			makeCurrent: function(el, current) {
				$(menu.items).removeClass('active');
				$(el).addClass('active');
				$(menu.subPanes).slideUp('fast');
				$(current).slideDown('fast');
				return false;
			},
			removeCurrent: function(el, current) {
				$(menu.items).removeClass('active');
				$(menu.subPanes).slideUp('fast');
				return false;
			},
			switchMenu: {
				"on": function(button) {
					
					$(mainPane).css({left: -menuWidth});

					$(button).data('status', "off");
					$('body').addClass('pinned').animate({left: menuWidth}, 600);
					$(mainPane).stop().animate({ left: 0 }, 600);
					$(menu.wrapper).stop().fadeIn();
				},
				"off": function(button) {
					//console.log("off");
					$(button).data('status', "on");
					$('body').animate({left: 0}, 600, function() {
						$(this).removeClass('pinned');
					});
					$(mainPane).stop().animate({ left: -menuWidth });
					$(menu.wrapper).fadeOut();
				}
			}
		}

		menu.init();

		$(window).on('resize', function() {
			menu.init();
		});

		$(menu.settings.switcher).on('click', function() {
			menu.methods.switchMenu[$(this).data('status')](this);
		});

		$(menu.items).on('click', function() {
			var currentSubPane = $(this).find(menu.subPanes);
			($(this).hasClass('active') == true) 
				? menu.methods.removeCurrent(this, currentSubPane) 
				: menu.methods.makeCurrent(this, currentSubPane);
		});


	}

})(jQuery);